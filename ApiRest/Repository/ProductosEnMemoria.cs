﻿using ApiRest.Model;

namespace ApiRest.Repository
{
	public class ProductosEnMemoria: IProductosEnMemoria
	{
		private readonly List<Producto> productos = new()
		{
			new Producto{ Id = 1, Nombre = "Iphaaaone", Descripción= "Modelos Pro", Precio = 5000.00, FechaAlta = DateTime.Now, SKU = "00001" },
			new Producto{ Id = 2, Nombre = "Macaa", Descripción= "Modelos Pro", Precio = 12000.00, FechaAlta = DateTime.Now, SKU = "00002"},
			new Producto{ Id = 3, Nombre = "MacMinaai", Descripción= "Chip M3", Precio = 4000.00, FechaAlta = DateTime.Now, SKU = "00003"},
			new Producto{ Id = 4, Nombre = "IMaaac", Descripción= "Pantalla 4k", Precio = 7000.00, FechaAlta = DateTime.Now, SKU = "00004"},
		}; 

		public IEnumerable<Producto> GetProductsAsincrono	() {
			return productos;
		}

		public Producto GetProductIdAsincrono(string sku) => productos.SingleOrDefault(p => p.SKU == sku);

		public void CreateProductAsincrono(Producto p)
		{
			productos.Add(p);
		}

		public void UpdateProductAsincrono(Producto p)
		{
			int indice = productos.FindIndex(existsProduct => existsProduct.Id == p.Id);
			productos[indice] = p;
		}

		public void DeleteProductAsincrono(string sku)
		{
			int indice = productos.FindIndex(existsProduct => existsProduct.SKU == sku);
			productos.RemoveAt(indice);
		}
	}
}
