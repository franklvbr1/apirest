﻿using ApiRest.Model;

namespace ApiRest.Repository
{
	public interface IProductosEnMemoria
	{
		Task<IEnumerable<Producto>> GetProductsAsincrono();
		Task<Producto> GetProductIdAsincrono(string sku);
		Task CreateProductAsincrono(Producto p);
		Task  UpdateProductAsincrono(Producto p);
		Task  DeleteProductAsincrono(string sku);
	}
}
