﻿using ApiRest.Model;
using NLog.Fluent;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ApiRest.Repository
{
	public class ProductSqlServer : IProductosEnMemoria
	{
		private readonly string CadenaConexion;
		private readonly ILogger<ProductSqlServer> log;
		private readonly StackFrame stackFrame = new(0);
		public ProductSqlServer(DataAcces cadenaConexion, ILogger<ProductSqlServer> l)
        {
			CadenaConexion = cadenaConexion.CadenaConexionSql;
			this.log = l;

		}
        private SqlConnection Conexion()
		{
			return new SqlConnection(CadenaConexion);
		}
		public async Task<IEnumerable<Producto>> GetProductsAsincrono()
		{
			string currentMethod = stackFrame.GetMethod().Name;
			SqlConnection sqlConnection = Conexion();
			SqlCommand cmd = null;
			List<Producto> lstproducto = new();

			try
			{
				sqlConnection.Open();
				cmd = sqlConnection.CreateCommand();
				cmd.CommandText = "GetProducts";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add("@SKU", SqlDbType.VarChar, 100).Value = null;
				SqlDataReader reader = await cmd.ExecuteReaderAsync();
				while (reader.Read())
				{
					lstproducto.Add(new Producto()
					{
						Nombre = reader["Nombre"].ToString(),
						Descripción = reader["Descripción"].ToString(),
						Precio = Convert.ToDouble(reader["Precio"].ToString()),
						SKU = reader["SKU"].ToString(),
					});
				}
			}
			catch (Exception ex)
			{
				log.LogError("Method {currentMethod} ", currentMethod + " " + ex.ToString());
				throw new ArgumentNullException("Se produjo un error al obtener el producto por SKU " + ex.Message);
			}
			finally
			{
				cmd.Dispose();
				sqlConnection.Close();
				sqlConnection.Dispose();
			}
			return lstproducto;
		}

		public  async Task<Producto> GetProductIdAsincrono(string sku)
		{
			string currentMethod = stackFrame.GetMethod().Name;
			SqlConnection sqlConnection = Conexion();
			SqlCommand cmd = null;
			Producto producto = null;

			try
			{
				sqlConnection.Open();
				cmd = sqlConnection.CreateCommand();
				cmd.CommandText = "GetProducts";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add("@SKU", SqlDbType.VarChar, 100).Value = sku;
				SqlDataReader reader = await cmd.ExecuteReaderAsync();
				if (reader.Read())
				{
					producto = new Producto
					{
						Nombre = reader["Nombre"].ToString(),
						Descripción = reader["Descripción"].ToString(),
						Precio = Convert.ToDouble(reader["Precio"].ToString()),
						SKU = reader["SKU"].ToString(),
					};
				}
			}
			catch (Exception ex)
			{
				log.LogError("Method {currentMethod} ", currentMethod + " " + ex.ToString());
				throw new ArgumentNullException("Se produjo un error al obtener el producto por SKU " + ex.Message);
			}
			finally
			{
				cmd.Dispose();
				sqlConnection.Close();
				sqlConnection.Dispose();
			}
			return producto;
		}
		public async Task CreateProductAsincrono(Producto p)
		{
			
			string currentMethod = stackFrame.GetMethod().Name.ToString();

			SqlConnection sqlConnection = Conexion();
			SqlCommand cmd = null;

			try
			{
				sqlConnection.Open();
				cmd = sqlConnection.CreateCommand();
				cmd.CommandText = "CreateProducts";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add("@Nombre", SqlDbType.VarChar, 500).Value = p.Nombre;
				cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 5000).Value = p.Descripción;
				cmd.Parameters.Add("@Precio", SqlDbType.Float).Value = p.Precio;
				cmd.Parameters.Add("@SKU", SqlDbType.VarChar, 100).Value = p.SKU;
				await cmd.ExecuteNonQueryAsync();
			}
			catch (Exception ex)
			{
				log.LogError("Method {currentMethod} ", currentMethod + " " + ex.ToString());
				throw new ArgumentNullException("Se produjo un error al dar de alta " + ex.Message);
			}
			finally { 
				cmd.Dispose();
				sqlConnection.Close();
				sqlConnection.Dispose();
			}
			await Task.CompletedTask;
		}
		public async Task UpdateProductAsincrono(Producto p)
		{
			string currentMethod = stackFrame.GetMethod().Name;
			SqlConnection sqlConnection = Conexion();
			SqlCommand cmd = null;

			try
			{
				sqlConnection.Open();
				cmd = sqlConnection.CreateCommand();
				cmd.CommandText = "UpdateProducts";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add("@Nombre", SqlDbType.VarChar, 500).Value = p.Nombre;
				cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 5000).Value = p.Descripción;
				cmd.Parameters.Add("@Precio", SqlDbType.Float).Value = p.Precio;
				cmd.Parameters.Add("@SKU", SqlDbType.VarChar, 100).Value = p.SKU;
				await cmd.ExecuteNonQueryAsync();
			}
			catch (Exception ex)
			{
				log.LogError("Method {currentMethod} ", currentMethod + " " + ex.ToString());
				throw new ArgumentNullException("Se produjo un error al actualizar el producto " + ex.Message);
			}
			finally
			{
				cmd.Dispose();
				sqlConnection.Close();
				sqlConnection.Dispose();
			}
			await Task.CompletedTask;
		}
		public async Task DeleteProductAsincrono(string sku)
		{
			string currentMethod = stackFrame.GetMethod().Name;
			SqlConnection sqlConnection = Conexion();
			SqlCommand cmd = null;

			try
			{
				sqlConnection.Open();
				cmd = sqlConnection.CreateCommand();
				cmd.CommandText = "DeleteProducts";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add("@SKU", SqlDbType.VarChar, 100).Value = sku;
				await cmd.ExecuteNonQueryAsync();
			}
			catch (Exception ex)
			{
				log.LogError("Method {currentMethod} ", currentMethod + " " + ex.ToString());
				throw new ArgumentNullException("Se produjo un error al borrar el producto " + ex.Message);
			}
			finally
			{
				cmd.Dispose();
				sqlConnection.Close();
				sqlConnection.Dispose();
			}
			await Task.CompletedTask;
		}
		
	}
}
