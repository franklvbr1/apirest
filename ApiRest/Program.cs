using ApiRest.Repository;
using NLog.Extensions.Logging;


var builder = WebApplication.CreateBuilder(args);

//builder.Host.ConfigureLogging((hostingContext, logging)=>
//{
//	logging.AddNLog();
//});
//add log
builder.Logging.AddNLog();
// Add services to the container.
var Configuration = new ConfigurationBuilder()
	.AddJsonFile("appsettings.json",optional:true, reloadOnChange:true)
	.Build();	
var CadenaConexionSqlServerConfiguration =  new DataAcces(Configuration.GetConnectionString("SQL"));
builder.Services.AddSingleton(CadenaConexionSqlServerConfiguration);

builder.Services.AddControllers(options=>
{
	options.SuppressAsyncSuffixInActionNames = false;
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//inyecatmos la interfas con nuestra clase para poder usarlo en cualquier controlador del proyecto
//builder.Services.AddSingleton<IProductosEnMemoria, ProductosEnMemoria>();
builder.Services.AddSingleton<IProductosEnMemoria, ProductSqlServer>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
