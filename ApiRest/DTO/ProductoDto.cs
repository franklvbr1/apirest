﻿using System.ComponentModel.DataAnnotations;

namespace ApiRest.DTO
{
	public class ProductoDto
	{
		[Required]
		public string Nombre { get; set; }
		[Required]
		public string Descripción { get; set; }
		[Required]
		[Range(1000,10000,
			ErrorMessage ="Valor {0} debe estar entre {1} y {2}.")]
		public double Precio { get; set; }
		public DateTime FechaAlta { get; init; }
		[Required]
		public string SKU { get; init; }
	}
}
