﻿using System.ComponentModel.DataAnnotations;

namespace ApiRest.DTO
{
	public class UpdateProductDto
	{
		[Required]
		public string Nombre { get; set; }
		[Required]
		public string Descripción { get; set; }
		[Required]
		[Range(1000, 10000,
			ErrorMessage = "Valor {0} debe estar entre {1} y {2}.")]
		public double Precio { get; set; }
	}
}
