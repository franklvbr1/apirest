﻿namespace ApiRest.Model
{
	public class Producto
	{
		public int Id { get; init; }
		public string Nombre { get; set; }
		public string Descripción { get; set; }
		public double Precio { get; set; }
		public DateTime FechaAlta { get; init; }
		public string SKU { get; init; }
	}
}
