﻿using ApiRest.DTO;
using ApiRest.Model;

namespace ApiRest
{
	public static class Utilities
	{
		public static ProductoDto ConverterDto(this Producto p) {
			return (p != null) ? new ProductoDto
			{
				Nombre = p.Nombre,
				Descripción = p.Descripción,
				Precio = p.Precio,
				SKU = p.SKU
			} : null;
		}
	}
}
