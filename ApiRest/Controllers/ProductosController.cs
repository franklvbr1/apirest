﻿using ApiRest.DTO;
using ApiRest.Model;
using ApiRest.Repository;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace ApiRest.Controllers
{
	[ApiController]
	[Route("[Controller]")]
	
	public class ProductosController : ControllerBase
	{
		private readonly IProductosEnMemoria _productosEnMemoria;

		public ProductosController(IProductosEnMemoria productosEnMemoria)
		{
			_productosEnMemoria = productosEnMemoria;
		}

		[HttpGet]
		public async Task< ActionResult<IEnumerable<ProductoDto>>> GetProductos()
		{
			var listProduct = (await _productosEnMemoria.GetProductsAsincrono()).Select(p=>p.ConverterDto());
			return Ok(listProduct);
		}

		[HttpGet("{codProducto}")]

		public async Task<ActionResult<ProductoDto>> GetProductoId(string codProducto)
		{
			var producto = (await _productosEnMemoria.GetProductIdAsincrono(codProducto)).ConverterDto();

			return (producto is null) ? NotFound() : producto;

		}

		[HttpPost]
		public async Task<ActionResult<ProductoDto>> CreateProduct(ProductoDto p)
		{
			Producto producto = new()
			{
				Nombre = p.Nombre,
				Descripción = p.Descripción,
				Precio = p.Precio,
				FechaAlta = DateTime.Now,
				SKU = p.SKU
			};
			await _productosEnMemoria.CreateProductAsincrono(producto);
			return producto.ConverterDto();
		}

		[HttpPut]

		public async Task<ActionResult<ProductoDto>> UpdateProducto(string idProducto, UpdateProductDto p)
		{
			Producto existsProducto = await _productosEnMemoria.GetProductIdAsincrono(idProducto);
			if (existsProducto is null)
			{
				return NotFound();
			}
			existsProducto.Nombre = p.Nombre;
			existsProducto.Descripción = p.Descripción;
			existsProducto.Precio = p.Precio;

			
			await _productosEnMemoria.UpdateProductAsincrono(existsProducto);

			return existsProducto.ConverterDto();
		}

		[HttpDelete]

		public async Task<ActionResult> DeleteProducto(string idProducto)
		{
			Producto existsProducto = await _productosEnMemoria.GetProductIdAsincrono(idProducto);
			if (existsProducto is null)
			{
				return NotFound();
			}
			await _productosEnMemoria.DeleteProductAsincrono(idProducto);
			return NoContent();
		}

	}
}
