/**************************************
***********FOR CREATE PRODUCT**********
***************************************/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='CreateProducts') 
BEGIN
	EXEC('CREATE PROCEDURE [dbo].[CreateProducts] AS')
	PRINT 'CREATE METHOD CreateProducts'
END
ELSE
BEGIN
	PRINT 'MODIFY METHOD CreateProducts'
END
GO

alter procedure dbo.CreateProducts
@Nombre varchar(500),
@Descripcion varchar(5000),
@Precio float,
@SKU varchar(100)
as
begin

-----------------
--##VERSION##:1
-----------------

if(select count(*) from Products where SKU = @SKU) > 0
	begin
		raiserror('El SKU ya existe en el sistema',16,1)
		return -1
	end
	
	insert into dbo.Products(Nombre,Descripción,Precio,FechaAlta,SKU) 
	values(@Nombre,@Descripcion,@Precio,getdate(),@SKU)

end
go


DECLARE @procedureName VARCHAR(200) = 'CreateProducts';
DECLARE @schemaName VARCHAR(100) = N'dbo';
EXEC DROP_SP_EXTENDEDPROPERTIES @procedureName;

-- PROCEDURE
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'CREATE PRODUCTS', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC SYS.sp_addextendedproperty @name = N'Unit',			@value = N'ApiRest', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC sys.sp_addextendedproperty @name = N'Version',			@value = N'1', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
-- PARAMETERS
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'NOMBRE',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@Nombre';
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'DESCRIPCION',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@Descripcion';
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'PRECIO',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@Precio';
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'SKU',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@SKU';
go

/**************************************
***********FOR LIST PRODUCTS**********
--EXAMPLE: EXEC GetProducts '00003'
***************************************/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='GetProducts') 
BEGIN
	EXEC('CREATE PROCEDURE [dbo].[GetProducts] AS')
	PRINT 'CREATE METHOD GetProducts'
END
ELSE
BEGIN
	PRINT 'MODIFY METHOD GetProducts'
END
GO

alter procedure dbo.GetProducts
@SKU varchar(100) = NULL
as
begin

-----------------
--##VERSION##:1
-----------------

select * from Products where @SKU IS NULL OR SKU = @SKU	

end
go

DECLARE @procedureName VARCHAR(200) = 'GetProducts';
DECLARE @schemaName VARCHAR(100) = N'dbo';
EXEC DROP_SP_EXTENDEDPROPERTIES @procedureName;

-- PROCEDURE
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'GET PRODUCTS', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC SYS.sp_addextendedproperty @name = N'Unit',			@value = N'ApiRest', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC sys.sp_addextendedproperty @name = N'Version',			@value = N'1', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
-- PARAMETERS
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'SKU',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@SKU';
go


/**************************************
***********FOR UPDATE PRODUCT**********
***************************************/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='UpdateProducts') 
BEGIN
	EXEC('CREATE PROCEDURE [dbo].[UpdateProducts] AS')
	PRINT 'CREATE METHOD UpdateProducts'
END
ELSE
BEGIN
	PRINT 'MODIFY METHOD UpdateProducts'
END
GO

alter procedure dbo.UpdateProducts
@Nombre varchar(500),
@Descripcion varchar(5000),
@Precio float,
@SKU varchar(100)
as
begin

-----------------
--##VERSION##:1
-----------------

if(select count(*) from Products where SKU = @SKU) = 0
	begin
		raiserror('El SKU no existe en el sistema',16,1)
		return -1
	end
	
	update dbo.Products 
	set  Nombre=@Nombre
		,Descripción=@Descripcion
		,Precio=@Precio
		,FechaAlta=getdate()
	where SKU = @SKU

end
go


DECLARE @procedureName VARCHAR(200) = 'UpdateProducts';
DECLARE @schemaName VARCHAR(100) = N'dbo';
EXEC DROP_SP_EXTENDEDPROPERTIES @procedureName;

-- PROCEDURE
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'UPDATE PRODUCTS', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC SYS.sp_addextendedproperty @name = N'Unit',			@value = N'ApiRest', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC sys.sp_addextendedproperty @name = N'Version',			@value = N'1', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
-- PARAMETERS
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'NOMBRE',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@Nombre';
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'DESCRIPCION',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@Descripcion';
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'PRECIO',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@Precio';
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'SKU',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@SKU';
go

/**************************************
***********FOR DELETE PRODUCT**********
***************************************/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='DeleteProducts') 
BEGIN
	EXEC('CREATE PROCEDURE [dbo].[DeleteProducts] AS')
	PRINT 'CREATE METHOD DeleteProducts'
END
ELSE
BEGIN
	PRINT 'MODIFY METHOD DeleteProducts'
END
GO

alter procedure dbo.DeleteProducts
@SKU varchar(100)
as
begin

-----------------
--##VERSION##:1
-----------------

if(select count(*) from Products where SKU = @SKU) = 0
	begin
		raiserror('El SKU no existe en el sistema',16,1)
		return -1
	end
	
	delete dbo.Products where SKU = @SKU

end
go


DECLARE @procedureName VARCHAR(200) = 'DeleteProducts';
DECLARE @schemaName VARCHAR(100) = N'dbo';
EXEC DROP_SP_EXTENDEDPROPERTIES @procedureName;

-- PROCEDURE
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'DELETE PRODUCTS', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC SYS.sp_addextendedproperty @name = N'Unit',			@value = N'ApiRest', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
EXEC sys.sp_addextendedproperty @name = N'Version',			@value = N'1', @level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName;
-- PARAMETERS
EXEC sys.sp_addextendedproperty @name = N'MS_Description',	@value = N'SKU',@level0type = N'SCHEMA', @level0name = @schemaName, @level1type = N'PROCEDURE', @level1name = @procedureName, @level2type = N'PARAMETER', @level2name = N'@SKU';
go