CREATE PROCEDURE [dbo].[DROP_SP_EXTENDEDPROPERTIES]  
 @P_PROCEDURE VARCHAR(200)  
AS  
BEGIN  
 -----------------  
 --##VERSION##: 1  
 -----------------  
 DECLARE SQL_COMMANDS CURSOR FOR  
  
 -- Scripts de borrado de propiedades extendidas de los parámetros del procedimiento.  
 SELECT   
  'EXEC sys.sp_dropextendedproperty @level0type = N''SCHEMA''  
  , @level0name = [' + SCH.name + ']  
  , @level1type = ''PROCEDURE''  
  , @level1name = [' + P.NAME + ']  
  , @level2type = ''PARAMETER''  
  , @level2name = [' + PAR.NAME + ']  
  , @name = ''' + REPLACE(CAST(XP.NAME AS NVARCHAR(300)), '''', '''''') + '''' AS SQL_COMMAND  
 FROM SYS.EXTENDED_PROPERTIES XP  
  INNER JOIN SYS.PROCEDURES P ON XP.MAJOR_ID = P.OBJECT_ID   
  INNER JOIN SYS.SCHEMAS SCH ON P.SCHEMA_ID = SCH.SCHEMA_ID  
  INNER JOIN SYS.PARAMETERS PAR ON XP.MAJOR_ID = PAR.OBJECT_ID AND XP.MINOR_ID = PAR.PARAMETER_ID  
 WHERE XP.CLASS_DESC = N'PARAMETER'  
  AND XP.MAJOR_ID = OBJECT_ID(@P_PROCEDURE)  
  
 -- Scripts de borrado de propiedades extendidas del procedimiento.  
 UNION  
 SELECT 'EXEC sp_dropextendedproperty @name = ''' + XP.NAME + '''  
  , @level0type = ''schema''  
  , @level0name = ''' + OBJECT_SCHEMA_NAME(XP.MAJOR_ID) + '''  
  , @level1type = ''procedure''  
  , @level1name = ''' + OBJECT_NAME(XP.MAJOR_ID) + '''' AS SQL_COMMAND  
 FROM SYS.EXTENDED_PROPERTIES XP  
  INNER JOIN SYS.PROCEDURES P on XP.MAJOR_ID = P.OBJECT_ID  
 WHERE XP.CLASS_DESC = 'OBJECT_OR_COLUMN'  
  AND XP.MINOR_ID = 0  
  AND XP.MAJOR_ID = OBJECT_ID(@P_PROCEDURE)  
  
 -- Ejecución del borrado  
 DECLARE @SQL_COMMAND AS VARCHAR(MAX)  
  
 OPEN SQL_COMMANDS  
 FETCH NEXT FROM SQL_COMMANDS INTO @SQL_COMMAND  
 WHILE @@FETCH_STATUS = 0  
 BEGIN  
  EXEC(@SQL_COMMAND)  
  FETCH NEXT FROM SQL_COMMANDS INTO @SQL_COMMAND  
 END  
  
 CLOSE SQL_COMMANDS  
 DEALLOCATE SQL_COMMANDS  
END 